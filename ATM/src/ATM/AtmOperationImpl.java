package src.ATM;

import java.util.Map;
import java.util.HashMap;

public class AtmOperationImpl implements AtmOperationInterface {
    ATM atm = new ATM();
    Map<Double, String> ministmt = new HashMap<>();
    @Override
    public void viewBalance() {
        System.out.println("Доступный баланс : " + atm.getBalance());
    }

    @Override
    public void withdrawAmount(double withdrawAmount) {
       if (withdrawAmount<=atm.getBalance()){
        ministmt.put(withdrawAmount, " Снятая сумма");
        System.out.println("Заберите деньги " + withdrawAmount  );
        atm.setBalance(atm.getBalance() - withdrawAmount);
        viewBalance();
       }
       else{
        System.out.println("Недостаточно средств");
       }
       
    }

    @Override
    public void depositAmount(double depositAmount) {
        ministmt.put(depositAmount, " Внесенная сумма");
        System.out.println(depositAmount + "  Успешно депанировано");
        atm.setBalance(atm.getBalance() + depositAmount);
        viewBalance();
    }   

    @Override
    public void viewMiniStatment() {
       for (Map.Entry<Double, String> m:ministmt.entrySet()){
        System.out.println(m.getKey() + "" + m.getValue());
       }
    }
   
}
