package src.ATM;
import java.util.Scanner;
import java.util.HashMap;

public class MainClass {
    public static void main(String[] args){
        AtmOperationInterface op = new AtmOperationImpl();
        int atmnumber = 12345;
        int atmpin=1234;
        Scanner in = new Scanner(System.in);
        System.out.println("Добро пожаловать!");
        System.out.print("Введите номер АТМ : ");
        int atmNumber = in.nextInt();
        System.out.print("Введите пин-код : ");
        int pin = in.nextInt();
        if((atmnumber==atmNumber)&&(atmpin==pin)){
            while (true) {
                System.out.println("1.Баланс\n2.Сумма вывода\n3.Сумма вклада\n4.Просмотр стаистики\n5.Выход");
                System.out.println("Выберите : ");
                int ch=in.nextInt();
                if (ch==1){
                    op.viewBalance();
                }
                else if (ch==2){
                    System.out.println("Введите сумму для вывода");
                    double withdrawAmount = in.nextDouble();
                    op.withdrawAmount(withdrawAmount);
                }
                else if (ch==3){
                    System.out.println("Введите сумму для внесения депозита : ");
                    double depositAmount = in.nextDouble();
                    op.depositAmount(depositAmount);
                }
                else if (ch==4){
                    op.viewMiniStatment();
                }
                else if (ch==5){
                    System.out.println("Заберите АТМ карту\n Спасибо за использование АТМ");
                    System.exit(0);
                }
                else {
                    System.out.println("Пожалуйста введите корректный выбор");
                }

            }
        }else{
            System.out.println("Неправильный номер АТМ или пин-код");
            System.exit(0);
        }
    }
}
